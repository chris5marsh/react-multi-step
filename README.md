# Multi-step form using React

Based on [Netsells' Javascript technical test](https://github.com/netsells/javascript-technical-test)

## Overview of tech stack

* React (for keeping track of state and updating UI)
* NextJS (for code splitting, bundling, Sass parsing and caching)
* Semantic UI (for layout)
* Axios - for HTTP requests (to send the data to the endpoint)

## Getting started

Download or clone the directory, and run `yarn` (you may need to [install Yarn first](https://yarnpkg.com/lang/en/docs/install/#windows-stable) by running `brew install yarn` on Mac or downloading the Windows .msi). This will install all the packages you need.

To test locally, run `npm run dev` in the command line and the site will appear at `localhost:3000`.

Testing tips:

* to start on a page other than the Intro screen, change the _step_ state in _pages/index.js_: `1` is _Name_, `2` is _Contact details_, and so on
* to pre-fill any field, update the relevant state in _components/MainForm.js_ (_live_in_uk_ should be `1` or `0`, and it's not ppossible to set a default value for _cv_ or _cover_letter_)

## How it works

I'm using React as it's a technology I'm currently getting to grips with, and wanted to challenge myself. It probably would have been faster to code up a regular form and show/hide sections using jQuery, and to submit the form that way, but wheres the fun in that?!

Most of the heavy lifting is done in _MainForm_: keeping the form answers in state, and loading up the correct stage in the form. The current _step_ you're on is hoisted to _index.js_, which keeps track of where you are in the process. This means _step_ can be shared between _MainForm_ and _Progress_.

Each stage of the form has it's own component(_Intro_, _Name_, _ContactDetails_, _UploadFiles_, _AboutYou_ and _Submit_), which handle error checking and laying out the form. _Submit_ also handles sending the data to the server and dealing with the response.

To speed things up, I'm using [Semantic UI](https://semantic-ui.com/), which has a [React integration](https://react.semantic-ui.com/). Semantic UI is a reasonably unopinionated and easily-themed UI toolkit, which makes it easy to add additional components (for example, _Progress_ uses a standalone Sass file to style it).

And finally, to send it through to the server and receive a response, I'm using [Axios](https://github.com/axios/axios), which neatens the Fetch API and includes Promises too.

## Deploying

I have set up a Netlify instance at https://react-multi-step-form.netlify.com/ which is automatically deployed when _master_ is committed. It uses the `next export` command to build a static version of the app.

[![Netlify Status](https://api.netlify.com/api/v1/badges/8f6dc2d7-72f5-40e5-9ba5-b8fc3ece19e3/deploy-status)](https://app.netlify.com/sites/react-multi-step-form/deploys)

## Future enhancements

* Error/success reporting on submit
* Further checks on text and file inputs
* Check git_profile format - bitbucket.com/chris5marsh came back as invalid
* Check that about_you is > 100 chars
* Browser testing
* Unit testing
* NoJS version? Single page web form that POSTs to the endpoint