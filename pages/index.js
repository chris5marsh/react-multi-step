import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';
import Meta from '../components/Meta';
import Progress from '../components/Progress';
import MainForm from '../components/MainForm';
import '../sass/main.scss';

class Home extends Component {

  // Set what step we're at at the top level
  state = {
    step: 0,
  }

  // Go to arbitrary step number
  goToStep = (i) => {
    // Sanity checks no bigger than 5 and no smaller than 0
    i = Math.min(Math.max(i, 0), 5);
    this.setState({
      step: i,
    });
  }

  render() {

    const { step } = this.state;
    const childProps = {
      step,
      goToStep: this.goToStep,
    };

    return (
      <div>
        <Meta />
        <Container className="main">
          <Progress {...childProps} />
          <MainForm {...childProps} />
        </Container>
      </div>
    );
  }

}

export default Home;
