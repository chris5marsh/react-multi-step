import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Message, Checkbox, Input } from 'semantic-ui-react'

class ContactDetails extends Component {

  // Props passed from parent component
  static propTypes={
    step: PropTypes.number.isRequired,
    goToStep: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    handleCheck: PropTypes.func.isRequired,
    email: PropTypes.string.isRequired,
    phone_number: PropTypes.string.isRequired,
    live_in_uk: PropTypes.string.isRequired,
    git_profile: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props);
    let liveInUkValue = props.live_in_uk;
    this.state = {
      emailError: false,
      liveInUkError: false,
      gitProfileError: false,
      errorType: false,
      liveInUkValue: liveInUkValue
    };
  }

  saveAndContinue = (e) => {
    e.preventDefault();
    let hasError = false;
    const emailInput = document.getElementById('email');
    const gitProfileInput = document.getElementById('git_profile');
    const emailRegex = RegExp('.*@.*'); // Simplest regex for email
    if (emailInput.value === '') {
      hasError = true;
      this.setState({
        emailError: true,
        errorType: 'empty'
      });
    } else if (emailRegex.test(emailInput.value) === false) {
      hasError = true;
      this.setState({
        emailError: true,
        errorType: 'validation'
      });
    } else {
      this.setState({
        emailError: false,
        errorType: false
      });
    }
    if (this.state.liveInUkValue === '') {
      hasError = true;
      this.setState({
        liveInUkError: true,
        errorType: 'empty'
      });
    } else {
      this.setState({
        liveInUkError: false,
        errorType: false
      });
    }
    if (gitProfileInput.value === '') {
      hasError = true;
      this.setState({
        gitProfileError: true,
        errorType: 'empty'
      });
    } else {
      this.setState({
        gitProfileError: false,
        errorType: false
      });
    }
    if (hasError === false) {
      this.props.goToStep(this.props.step+1);
    }
  }

  goBack = (e) => {
    e.preventDefault()
    this.props.goToStep(this.props.step-1);
  }

  handleCheck = (e, { value }) => {
    this.setState({
      liveInUkValue: value
    });
    this.props.handleCheck('live_in_uk', value);
  }

  render() {
    const { email, phone_number, git_profile } = this.props;
    const { emailError, liveInUkError, gitProfileError, errorType, liveInUkValue } = this.state;
    const hasError = emailError || liveInUkError || gitProfileError;
    let errorMessage = '';
    if (errorType === 'validation') {
      errorMessage+= 'One of the values isn\'t in the expected format. ';
    } else if (errorType === 'empty') {
      errorMessage+= 'You must enter a value in the required fields.';
    }
    let gitProfileClassname = "required field";
    if (gitProfileError === true) {
      gitProfileClassname+= ' error';
    }
    return (
      <div>
        <h3>Contact details</h3>
        <Form error={hasError}>
          <Message
            hidden={!hasError}
            error={hasError}
            header='Please check the form'
            content={errorMessage}
          />
          <Form.Group widths='equal'>
            <Form.Input
              label='Enter your email address'
              onChange={this.props.handleInputChange('email')}
              id="email"
              defaultValue={email}
              type="email"
              required={true}
              error={emailError}
            />
            <Form.Input
              label='Enter your phone number (optional)'
              onChange={this.props.handleInputChange('phone_number')}
              id="phone_number"
              defaultValue={phone_number}
              type="number"
            />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Field error={liveInUkError}>
              <label htmlFor="live_in_uk">Do you live in the UK?</label>
              <Checkbox
                radio
                label='Yes'
                name='live_in_uk'
                id='1'
                value='1'
                checked={liveInUkValue === '1'}
                onChange={this.handleCheck}
              />
              <Checkbox
                radio
                label='No'
                name='live_in_uk'
                id='0'
                value='0'
                checked={liveInUkValue === '0'}
                onChange={this.handleCheck}
              />
            </Form.Field>
            <div className={gitProfileClassname}>
              <label htmlFor="git_profile">Enter your git profile</label>
              <Input
                placeholder='https://github.com/netsells'
                onChange={this.props.handleInputChange('git_profile')}
                id="git_profile"
                defaultValue={git_profile}
              />
            </div>
          </Form.Group>
          <Form.Group>
            <Form.Button onClick={this.goBack}>Back</Form.Button>
            <Form.Button primary onClick={this.saveAndContinue}>Next</Form.Button>
          </Form.Group>
        </Form>
      </div>
    )
  }
}

export default ContactDetails;