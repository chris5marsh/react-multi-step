import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Button, Message } from 'semantic-ui-react'

class Submit extends Component{

  // Props passed from parent component
  static propTypes={
    step: PropTypes.number.isRequired,
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone_number: PropTypes.string.isRequired,
    live_in_uk: PropTypes.string.isRequired,
    git_profile: PropTypes.string.isRequired,
    cv: PropTypes.object.isRequired,
    cover_letter: PropTypes.object.isRequired,
    about_you: PropTypes.string.isRequired,
    goToStep: PropTypes.func.isRequired
  }

  state={
    message: {
      type: false,
      header: false,
      content: false,
      list: []
    }
  }

  goBack = (e) => {
    e.preventDefault();
    this.props.goToStep(this.props.step-1);
  }

  dealWithError = (res) => {
    let messageContent = false;
    let messageList = false;
    if (res.data && res.data.errors) {
      messageList = [];
      Object.keys(res.data.errors).forEach((k) => {
        // Go through array of error statements
        res.data.errors[k].forEach((err) => messageList.push(err));
      });
    }
    // Show error message
    this.setState({
      message: {
        type: 'error',
        header: 'There was a problem with your submission.',
        content: messageContent,
        list: messageList
      }
    });
  }

  submitDetails = () => {
    const formData = new FormData();
    if (this.props.first_name !== '') formData.append("first_name", this.props.first_name);
    if (this.props.last_name !== '') formData.append("last_name", this.props.last_name);
    if (this.props.email !== '') formData.append("email", this.props.email);
    if (this.props.phone_number !== '') formData.append("phone_number", this.props.phone_number);
    if (this.props.live_in_uk !== null) formData.append("live_in_uk", this.props.live_in_uk);
    if (this.props.git_profile !== '') formData.append("git_profile", this.props.git_profile);
    if (this.props.cv.size && this.props.cv.size > 0) {
      formData.append("cv", this.props.cv);
    }
    if (this.props.cover_letter.size && this.props.cover_letter.size > 0) {
      formData.append("cover_letter", this.props.cover_letter);
    }
    if (this.props.about_you !== '') formData.append("about_you", this.props.about_you);
    var self = this;
    axios({
      method: "post",
      url: "https://recruitment-submissions.netsells.co.uk/api/vacancies/javascript-developer/submissions",
      data: formData,
      mode: "no-cors",
      headers: {
        "X-Requested-With": "XMLHttpRequest",
        "Content-Type": "multipart/form-data",
        "Accept": "application/json"
      }
    }).then(function(res) {
      if (res.status == 200) {
        // Show success message
        self.setState({
          message: {
            type: 'success',
            header: 'Congratulations',
            content: 'You have successfully submitted your details',
            list: []
          }
        });
      } else {
        self.dealWithError(res);
      }
    }).catch(function (error) {
      self.dealWithError(error.response);
    });
  }

  render() {
    const live_in_uk = this.props.live_in_uk === true ? 'Yes' : 'No' ;
    const {
      message
    } = this.state;
    return (
      <div>
        <h3>All done!</h3>
        <p>Please check through your details below and then click &lsquo;Submit&rsquo;.</p>
        <ul>
          <li>First name: <strong>{this.props.first_name}</strong></li>
          <li>Last name: <strong>{this.props.last_name}</strong></li>
          <li>Email: <strong>{this.props.email}</strong></li>
          <li>Phone number: <strong>{this.props.phone_number}</strong></li>
          <li>Do you live in the uk? <strong>{live_in_uk}</strong></li>
          <li>Git profile: <strong>{this.props.git_profile}</strong></li>
          <li>CV: <strong>{this.props.cv.name}</strong></li>
          <li>Cover letter: <strong>{this.props.cover_letter.name}</strong></li>
          <li>About you: <strong>{this.props.about_you}</strong></li>
        </ul>
        <Button onClick={this.goBack}>Back</Button>
        <Button primary onClick={this.submitDetails}>Submit your details</Button>
          <Message
            hidden={message.type === false}
            error={message.type === 'error'}
            success={message.type === 'success'}
            header={message.header}
            content={message.content}
            list={message.list}
          />
      </div>
    )
  }
}

export default Submit;