import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react'

const Intro = (props) => {

  const saveAndContinue = (e) => {
    e.preventDefault();
    props.goToStep(props.step+1);
  }

  return (
    <div>
      <h3>Welcome to our multi-step form!</h3>
      <p>To fill in this form you&rsquo;ll need:</p>
      <ul>
        <li>Your name, email address and phone number</li>
        <li>Some other contact details</li>
        <li>Your CV and cover letter, as a PDF or a Word doc</li>
        <li>A short bio</li>
      </ul>
      <Button primary onClick={saveAndContinue}>Get started</Button>
    </div>
  )

}

Intro.propTypes={
  step: PropTypes.number.isRequired,
  goToStep: PropTypes.func.isRequired,
}

export default Intro;