import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, TextArea, Message } from 'semantic-ui-react'

class AboutYou extends Component {

  // Props passed from parent component
  static propTypes={
    step: PropTypes.number.isRequired,
    goToStep: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    about_you: PropTypes.string.isRequired,
  }

  state={
    aboutYouError: false,
    aboutYouErrorMessage: '',
  }

  saveAndContinue = (e) => {
    e.preventDefault()
    const aboutYouTextArea = document.getElementById('about_you');
    if (aboutYouTextArea.value === '') {
      this.setState({
        aboutYouError: true,
        aboutYouErrorMessage: 'You must enter a value in the required fields.',
      });
    } else if (aboutYouTextArea.value.length < 100) {
      this.setState({
        aboutYouError: true,
        aboutYouErrorMessage: 'The content must be at least 100 characters',
      });
    } else {
      this.setState({
        aboutYouError: false,
      });
      this.props.goToStep(this.props.step+1);
    }
  }

  goBack = (e) => {
    e.preventDefault();
    this.props.goToStep(this.props.step-1);
  }

  render() {
    const { about_you } = this.props;
    const {
      aboutYouError,
      aboutYouErrorMessage
    } = this.state;
    let fieldClassname = "required field";
    if (aboutYouError === true) {
      fieldClassname+= ' error';
    }
    return (
      <div>
        <h3>Contact details</h3>
        <Form error={aboutYouError}>
          <Message
            hidden={!aboutYouError}
            error={aboutYouError}
            header='Please check the form'
            content={aboutYouErrorMessage}
          />
          <Form.Group widths='equal'>
            <div className={fieldClassname}>
              <label htmlFor="about_you">Tell us a little bit about yourself</label>
              <div className="ui input">
                <TextArea
                  onChange={this.props.handleInputChange('about_you')}
                  id="about_you"
                  defaultValue={about_you}
                  required={true}
                />
              </div>
            </div>
          </Form.Group>
          <Form.Group>
            <Form.Button onClick={this.goBack}>Back</Form.Button>
            <Form.Button primary onClick={this.saveAndContinue}>Next</Form.Button>
          </Form.Group>
        </Form>
      </div>
    )
  }
}

export default AboutYou;