import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../sass/progress.scss';

class Progress extends Component {

  // Props passed from parent component
  static propTypes={
    step: PropTypes.number.isRequired,
    goToStep: PropTypes.func.isRequired,
  }

  handleClick = e  => {
    e.preventDefault();
    const listItemsHTML = document.getElementsByClassName('progress__item');
    const listItems = [...listItemsHTML]; // convert to array
    // Get index of selected list item
    const index = listItems.indexOf(e.target)+1;
    this.props.goToStep(index);
  }

  render() {
    const { step } = this.props;
    return (
      <div className="progress">
        <ol className="progress__list">
          <li className={step === 1 ? "progress__item is-current" : "progress__item"} onClick={this.handleClick}>Your name</li>
          <li className={step === 2 ? "progress__item is-current" : "progress__item"} onClick={this.handleClick}>Contact details</li>
          <li className={step === 3 ? "progress__item is-current" : "progress__item"} onClick={this.handleClick}>Upload files</li>
          <li className={step === 4 ? "progress__item is-current" : "progress__item"} onClick={this.handleClick}>About you</li>
          <li className={step === 5 ? "progress__item is-current" : "progress__item"} onClick={this.handleClick}>Submit your details</li>
        </ol>
      </div>
    )
  }
}

export default Progress;
