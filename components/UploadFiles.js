import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Message } from 'semantic-ui-react'

class UploadFiles extends Component {

  // Props passed from parent component
  static propTypes={
    step: PropTypes.number.isRequired,
    goToStep: PropTypes.func.isRequired,
    handleFileChange: PropTypes.func.isRequired,
    cv: PropTypes.object.isRequired,
    cover_letter: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {
      cvError: false,
      coverLetterError: false,
      errorType: false,
    }
  }

  saveAndContinue = (e) => {
    e.preventDefault();
    const cvInput = document.getElementById('cv');
    if (cvInput.value === '') {
      this.setState({
        cvError: true,
        errorType: 'empty'
      });
    } else {
      this.setState({
        cvError: false,
        errorType: false
      });
      this.props.goToStep(this.props.step+1);
    }
  }

  runFileTests = (file) => {
    if (this.testFileSize(file) === false) return false;
    if (this.testFileExtension(file) === false) return false;
    return true;
  }

  testFileExtension = (file) => {
    const extension = file.name.split('.').pop();
    const allowedExtensions = [
      'pdf',
      'docx',
      'doc'
    ];
    if (allowedExtensions.indexOf(extension) === -1) {
      // Allowed file types
      this.setState({ errorType: 'filetype' });
      return false;
    }
    return true;
  }

  testFileSize = (file) => {
    if (file.size > 2097152) {
      // Max size 2MB
      this.setState({ errorType: 'filesize' });
      return false;
    }
    return true;
  }

  handleFileChange = (e) => {
    this.setState({
      cvError: false,
      coverLetterError: false,
      errorType: false
    });
    const file = e.target.files[0];
    const targetId = e.target.id;
    if (this.runFileTests(file) === false) {
      if (targetId === 'cv') this.setState({ cvError: true });
      if (targetId === 'cover_letter') this.setState({ coverLetterError: true });
    } else {
      this.props.handleFileChange(file, targetId);
    }
  }

  goBack = (e) => {
    e.preventDefault();
    this.props.goToStep(this.props.step-1);
  }

  render() {
    const {
      cvError,
      coverLetterError,
      errorType
    } = this.state;
    const hasError = cvError || coverLetterError;
    let errorMessage = '';
    if (errorType === 'filesize') {
      errorMessage = 'One of the files you have uploaded is too large.';
    } else if (errorType === 'filetype') {
      errorMessage = 'You must upload a PDF or Word doc (.doc or .docx)';
    } else if (errorType === 'empty') {
      errorMessage = 'You must enter a value in the required fields.';
    }
    return (
      <div>
        <h3>Upload files</h3>
        <Form error={hasError}>
          <Message
            hidden={!hasError}
            error={hasError}
            header='Please check the form'
            content={errorMessage}
          />
          <Form.Group widths='equal'>
            <Form.Input
              type="file"
              label='Upload your CV (in PDF or Word format, max 2MB)'
              onChange={this.handleFileChange}
              id="cv"
              required={true}
              error={cvError}
            />
            <Form.Input
              type="file"
              label='Upload your cover letter (optional, in PDF or Word format, max 2MB)'
              onChange={this.handleFileChange}
              id="cover_letter"
              error={coverLetterError}
            />
          </Form.Group>
          <Form.Group>
            <Form.Button onClick={this.goBack}>Back</Form.Button>
            <Form.Button primary onClick={this.saveAndContinue}>Next</Form.Button>
          </Form.Group>
        </Form>
      </div>
    )
  }
}

export default UploadFiles;