import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Message } from 'semantic-ui-react'

class FirstName extends Component {

  // Props passed from parent component
  static propTypes={
    step: PropTypes.number.isRequired,
    goToStep: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
  }

  state={
    firstNameError: false
  }

  saveAndContinue = (e) => {
    e.preventDefault();
    const firstNameInput = document.getElementById('first_name');
    if (firstNameInput.value === '') {
      this.setState({
        firstNameError: true
      });
    } else {
      this.setState({
        firstNameError: false
      });
      this.props.goToStep(this.props.step+1);
    }
  }

  goBack = (e) => {
    e.preventDefault();
    this.props.goToStep(this.props.step-1);
  }

  render() {
    const { first_name, last_name } = this.props;
    const { firstNameError } = this.state;
    return (
      <div>
        <h3>Your name</h3>
        <Form error={firstNameError}>
          <Message
            hidden={!firstNameError}
            error={firstNameError}
            header='Please check the form'
            content='You must enter a value in the required fields'
          />
          <Form.Group widths='equal'>
            <Form.Input
              label='Enter your first name'
              onChange={this.props.handleInputChange('first_name')}
              id="first_name"
              defaultValue={first_name}
              required={true}
              error={firstNameError}
            />
            <Form.Input
              label='Enter your last name (optional)'
              onChange={this.props.handleInputChange('last_name')}
              id="last_name"
              defaultValue={last_name}
            />
          </Form.Group>
          <Form.Group>
            <Form.Button onClick={this.goBack}>Back</Form.Button>
            <Form.Button primary onClick={this.saveAndContinue}>Next</Form.Button>
          </Form.Group>
        </Form>
      </div>
    )
  }
}

export default FirstName;