import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Intro from './Intro';
import Name from './Name';
import ContactDetails from './ContactDetails';
import UploadFiles from './UploadFiles';
import AboutYou from './AboutYou';
import Submit from './Submit';

class MainForm extends Component {

  // Props passed from parent component
  static propTypes={
    step: PropTypes.number.isRequired,
    goToStep: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      phone_number: '',
      live_in_uk: '',
      git_profile: '',
      cv: {},
      cover_letter: {},
      about_you: ''
    }
  }

  // Add values to state
  handleInputChange = input => event => {
    this.setState({ [input] : event.target.value })
  }

  // Different way to handle checkboxes
  handleCheck = (input, value) => {
    this.setState({ [input] : value })
  }

  handleFileChange = (file, type) => {
    if (type === 'cv') {
      this.setState({ cv: file });
    } else if (type === 'cover_letter') {
      this.setState({ cover_letter: file });
    }
  }

  render(){
    const {
      step,
      goToStep
    } = this.props;
    const { first_name, last_name, email, phone_number, live_in_uk, git_profile, cv, cover_letter, about_you } = this.state;
    const values={ first_name, last_name, email, phone_number, live_in_uk, git_profile, cv, cover_letter, about_you };
    switch(step) {
      case 0:
        return <Intro
          step={step}
          goToStep={goToStep}
          />
      case 1:
        return <Name
          step={step}
          goToStep={goToStep}
          handleInputChange={this.handleInputChange}
          {...values}
          />
      case 2:
        return <ContactDetails
          step={step}
          goToStep={goToStep}
          handleInputChange={this.handleInputChange}
          handleCheck={this.handleCheck}
          {...values}
          />
      case 3:
        return <UploadFiles
          step={step}
          goToStep={goToStep}
          handleFileChange={this.handleFileChange}
          cvFileInput={el => (this.cvFileInput = el)}
          coverLetterFileInput={el => (this.coverLetterFileInput = el)}
          {...values}
          />;
      case 4:
        return <AboutYou
          step={step}
          goToStep={goToStep}
          handleInputChange={this.handleInputChange}
          {...values}
          />
      case 5:
        return <Submit
          step={step}
          goToStep={goToStep}
          {...values}
        />
      default:
        return (
          <p>There was an error!</p>
        );
    }
  }
}

export default MainForm;